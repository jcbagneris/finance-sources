# Finance Sources

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a>
<a href="http://calver.org/"><img src="https://img.shields.io/badge/calver-YYYY.0M.MINOR-22bfda.svg"></a>

This repo holds the sources of the various handouts, slides or other material
that I have written for my finance courses over the last, hmm, ten years at
least. Yes, I know it is nearly empty: I am in the process of cleaning
everything before pushing here, so stay tuned and be patient...

Note that the git history will start when this repo was created (June 2017), but
some sources are older: either they were not versionned before, or versionned in
their own repositories. The old histories were usually not worth merging
here (mainly "fix typos" commits).

Most of the sources are latex or markdown ones, required helpers for compilation
(makefiles, includes and filters) are in the writing-helpers repository on
[github](https://github.com/jcbagneris/writing-helpers)
or [gitlab](https://gitlab.com/jcbagneris/writing-helpers).

If for some reason you need to access to the pdf versions, they are available on
<https://files.bagneris.net/>.

## Contributing

Found a typo? A spelling error? Another kind of mistake? Please let me know!

For the technically inclined, open an issue, submit a patch or fork the repo
and create a pull request. If this means nothing to you, just drop me an email,
telling me which source is affected, and where (preferably with the line
number). Thank you!

## License

This work is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International
License <http://creativecommons.org/licenses/by-nc-sa/4.0/>.
The terms of this license allow you to remix, tweak, and
build upon this work non-commercially, as long as you credit me
and license your new creations under the identical terms.
