#+TITLE: Myphone
#+export_file_name: ./_dest/20231020T141020--myphone-jc-bagneris__en_pb_inv

* preamble                                                                                  :ignore:
#+latex: \thispagestyle{empty}

* Text                                                                                      :ignore:
Myphone produces and sells innovating smartphones. These phones are currently
the only ones on the market that can be upgraded, repaired and customized at
will, thanks to their design: most parts can be chosen and assembled freely. The
customer can for example choose the optics (camera), the memory size, the screen
technology etc.

Myphone is considering developping a new model which would accept a physical
keyboard and even a small beamer, for the niche market of nomad software
developers and staff of "full remote" companies.

The investment would suppose the acquisition of new machines for a total amount
of 418,000€, and IT devices for 343,000€. The IT devices would be depreciated
straight line to zero over 7 years, and will be worthless at the end of the
project, which would last 4 years. The machines will be depreciated on a
straight line to zero basis over 11 years and will be sold to a broker for 34% of
their acquisition value at the end of the project.

The new phone will be sold for 480€. Producing and selling the phones would
entail fixed costs of 430,000€ per year. The variable costs are estimated to 250€
per phone. The company forecasted that it would sell 7,100 phones per year. The
working capital of the company will not be affected by this project.

Myphone is not listed on a financial market but the unlevered
beta of its sector is 0.97. The company issued 23,000 bonds privately (the bonds
were bought in totality by a venture capital company) 5 years ago. The bonds
have a 100€ face value and will be repaid with a 2.2% premium in 8 years from
now. The coupon rate is 4.1% and their current YTM is 3.74%. These are the
only debt of Myphone. Besides, the venture capital company estimated the value
of the equity of Myphone at 3.4 millions €.

Finally, the expected market return is 6.9% this year, and the risk free rate
0.9%. The corporate tax rate for Myphone is 32% and you may assume that the
company is profitable and pays taxes.

* Questions
1. What is the market value of Myphone's debt? (in €, no decimal places)
2. What is the required rate of return (RRR) on the investment project?
   (in %, 2 decimal places)
3. According to the data provided, what would be the yearly operating cash flow?
   (in €, no decimal places)
4. What would then be the NPV of the project? (in €, no decimal places)
5. Which minimum yearly operating cash flow would ensure a zero NPV for the
   project?
   (in €, no decimal places)
6. How many smartphones should be sold per year to get the minimum operating
   cash flow?
   (no decimal places)
7. To finance the project, Myphone is considering a 348,000€ bank loan over 6
   years, repaid by constant annuities, at the rate at which it can currently
   borrow. What would then be the value of the constant annuity? (in €, 2
   decimal places)
8. What would be the required rate of return of the project with the new loan?
   (in %, 2 decimal places)
   Assume that the cost of debt does not change (it is not affected).


* footer                                                                                    :ignore:
#+include: "~/work/writing-helpers/orgmode/layout.org::*footer-std" :minlevel 1 :only-contents t

* locvar                                                                                    :ignore:

# Local Variables:
# time-stamp-line-limit: 3
# time-stamp-start: "^#\\+export_file_name: .*/_dest/"
# time-stamp-format: "%Y%m%dT%H%M%S"
# time-stamp-end: "--"
# End:
