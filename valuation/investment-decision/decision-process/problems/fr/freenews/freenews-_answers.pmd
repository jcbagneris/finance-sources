## Questions et réponses {-}

**Quelle est la valeur de marché des capitaux propres de Freenews (en milliers d'euros ) ?**

Freenews a 10 millions d'actions dont le dernier cours boursier est 13,80 euros,
on a donc :
$$ C = 10\,000\,000 \times 13,8 = 138\,000\,000 $$

**Quel est le cours boursier actuel de l'obligation Freenews (en base 100) ?**

Il nous faut le nombre d'années restant à courir (maturité des obligations), le
taux de coupon, la valeur de remboursement et le taux de rendement des
obligations actuellement.

Les obligations ont été émises il y a 3 ans pour 7 ans, il reste donc 4 ans. En
base 100, le coupon annuel est de 6, et le remboursement sera de 102. Enfin,
l'entreprise peut actuellement emprunter à 6,8% par an, et c'est le taux qui
sera retenu pour le rendement brut des obligations. Finalement, les flux
attachés à une obligation sont (en base 100) :

\begin{center}
\begin{tabular}{lrrrr}
\toprule
Année & 1 & 2 & 3 & \multicolumn{1}{c}{4} \\
\otoprule
Flux & 6 & 6 & 6 & $6 + 102$ \\
\bottomrule
\end{tabular}
\end{center}

Et donc :
\begin{eqnarray*}
P0 & = & \frac{6}{1,068^1} + \frac{6}{1,068^2} + \frac{6}{1,068^3} +
\frac{6+102}{1,068^4} \\
& = & \sum_{t=1}^4 \frac{6}{1,068^t} + \frac{102}{1,068^4} \\
& = & 6 \times \frac{1-1,068^{-4}}{0,068} + \frac{102}{1,068^4} \\
& = & 98,815 \\
\end{eqnarray*}

La valeur d'une obligation en base 100 est 98,815. Pour une valeur nominale de 1
euro, le prix d'une obligation en euros est $1 \times 0,98815 = 0,98815$ euros.

**En déduire la valeur de marché des dettes de l'entreprise (en milliers d'euros)**

Il y a 100 millions d'obligations en circulation, et le prix d'une obligation
est 0,98815 euros. Donc :
$$ D = 100\,000\,000 \times 0,98815 = 98\,815\,000$$

**Quel est le bêta de Freenews ?**

On connaît le bêta du concurrent, et son levier financier (D/C). On peut donc en
déduire le bêta sans dettes du concurrent ($\beta_{SD}$). On rappelle que le
bêta sans dettes représente le bêta de l'activité uniquement, indépendamment du
financement. Il est clair que Freenews et son concurrent exercent la même
activité, puisqu'ils sont concurrents. Ils ont donc le même bêta sans dettes.

Finalement, on va calculer le bêta sans dettes à partir des données du
concurrent, puis l'utiliser pour trouver le bêta de Freenews à l'aide du levier
financier (D/C) de celle-ci. Le levier financier de Freenews est trouvé avec les
réponses des questions 1 (C) et 3 (D).

$$ \beta_{SD} = \frac{1,8}{1+0,42 \times(1-0,34)} = 1,4093 $$

Le levier financier (D/C) de Freenews est :
$$ \frac{D}{C} = \frac{98\,815\,000}{138\,000\,000} = 0,7161 $$

Finalement, le bêta de Freenews est donné par :
$$ \beta = 1,4093 \times \left[ 1+ 0,7161 \times(1-0,34) \right] = 2,0754 $$

**Quel rendement peut-on attendre du marché financier pour l'année en cours ?**

Le rendement attendu du marché financier ou $E(R_M)$ peut être obtenu à partir
de la formule du MEDAF avec les données du concurrent. Le taux libre de risque
ou $r_f$ est le rendement des emprunts d'État ou 4,1%.

La formule d'équilibre pour le concurrent est :
$$ 0,24 = 0,041 + 1,8 \times (E(R_m) - 0,041)$$

Le rendement attendu du marché financier est donc :
$$ E(R_m) = \frac{0,24-0,041}{1,8} + 0,041 = 0,1516 = 15,16\% $$

**Quel est le coût des capitaux propres de Freenews ?**

On dispose désormais de toutes les informations nécessaires pour utiliser le
MEDAF dans le cas de Freenews :
$$ k_C = 0,041 + 2,0754 \times (0,1516-0,041) = 0,2704 = 27,04\% $$

**En déduire la croissance annuelle attendue sur les dividendes de Freenews**

On connait le coût des capitaux propres ($k_C$) de Freenews, et on sait par
ailleurs que le prochain dividende sera de 1,8 par action cotant 13,8 euros. On
va donc utiliser le modèle de Gordan pour en déduire la croissance attendue sur
les dividendes de l'entreprise Freenews :
\begin{eqnarray*}
0,2704 & = & \frac{3,2}{13,8} + g \\
g & = & 0,2704 - \frac{3,2}{13,8} = 0,0386  = 3,86\% \\
\end{eqnarray*}

**Quel est le coût des dettes de Freenews, à utiliser dans le CMPC ?**

L'entreprise peut emprunter actuellement à 6,8%, donc :
$$ k_D = 0,068 \times (1-0,34) = 0,04488 = 4,49\% $$

**Quel est le CMPC de Freenews, à utiliser comme taux de rendement requis du projet ?**

Le tableau ci-dessous donne le détail du calcul du coût du CMPC.

\begin{center}
\begin{tabular}{lrrrr}
\toprule
Ressource & \multicolumn{1}{c}{Coût} & \multicolumn{1}{c}{Valeur} & Poids &  \\
\otoprule
Dettes           &  4,49\% &  98~815~000 & 0,4173 &  1,87\% \\
Capitaux propres & 27,04\% & 138~000~000 & 0,5827 & 15,76\% \\
\midrule
                 &         & 236~815~000 &   CMPC & 17,63\% \\
\bottomrule
\end{tabular}
\end{center}

**Si on suppose que le chiffre d'affaire annuel apporté par le projet représente 750 000 euros, quel est le flux d'activité qui en résulte ?**

Les actifs du projet ont une valeur de 360 000 et s'amortissent sur 4 ans, on a
donc une dotation aux amortissements annuelle de 90 000 euros. Les autres
valeurs sont tirées des caractéristiques du projet dans l'énoncé.

La variation du BFR est nulle puisque celui-ci reste constant à 75 000 euros.

\begin{center}
\begin{tabular}{lrr}
\toprule
Chiffre d'affaires        & 750~000 &      \\
Charges de personnel      & 450~000 &      \\
Autres dépenses           & 172~500 & 23\% \\
EBE                       & 127~500 &      \\
Dot. amortissements       &  90~000 &      \\
Imposable                 &  37~500 &      \\
\midrule
Impôt                     &  12~750 & 34\% \\
\midrule
Variation du BFR          &       0 &      \\
\midrule
Cash flow                 & 114~750 &      \\
\bottomrule
\end{tabular}
\end{center}

**Quel est le flux d'activité qui assure une VAN au moins égale à 0 ?**

La VAN est égale à la valeur actuelle des 4 flux d'activité annuels (ce que l'on
cherche), diminuée de l'investissement dans les actifs et en BFR. On suppose que
l'on récupère le BFR la quatrième année.

\begin{eqnarray*}
VAN     & = & - (360\,000 + 75\,000) + \sum_{t=1}^4 \frac{CF}{1,1763^t} +
\frac{75\,000}{1,1763^4} \\
435\,000 & = & \sum_{t=1}^4 \frac{CF}{1,1763^t} + \frac{75\,000}{1,1763^4}\\
435\,000 - 39\,170 & = & CF \times \frac{1-1,1763^{-4}}{0,1763} \\
CF      & = & 395\,830 \times \frac{0,1763}{1-1,1763^{-4}} \\
CF      & = & 146\,095 \\
\end{eqnarray*}


**En déduire le chiffre d'affaire annuel minimal nécessaire pour qu'il soit envisageable d'investir dans ce projet**

Soit CA le chiffre d'affaires recherché, CF le cash-flow annuel et IS l'impôt
sur les bénéfices :

\begin{eqnarray*}
CF & = & CA - 450\,000 - 0,23CA - IS \\
CF & = & 0,77CA - 450\,000 - IS \\
IS & = & \left( CA - 450\,000 - 0,23CA - 90\,000 \right) \times 0,34 \\
IS & = & \left( 0,77CA - 540\,000  \right) \times 0,34 \\
IS & = &  0,2618CA - 183\,600 \\
CF & = & 0,77CA - 450\,000 - \left(0,2618CA - 183\,600 \right)\\
CF & = & 0,5082CA - 266\,400 \\
CA  & = & \frac{CF + 266\,400}{0,5082} \\
CA & = & \frac{146\,095 + 266\,400}{0,5082} = 811\,678  \\
\end{eqnarray*}
