Freenews est un des premiers journaux gratuits lancés en France, puisque les
premières éditions ont été distribuées en région parisienne en 1999. Ce type de
journaux a pour seules recettes les recettes publicitaires, mais dispose d'une
rédaction tout à fait standard et fonctionne comme les autres quotidiens.

Début 2005, les dirigeants de Freenews envisagent d'investir pour consolider
leur position sur Internet, où ils sont un des sites de nouvelles générales en
langue française les plus consultés. Il s'agirait de mettre en place une
infrastructure permettant de proposer, en plus des articles traditionnels, des
*podcasts*, toutes les études montrant que ce type de support va se
généraliser dans les années à venir.

Ce projet devrait permettre d'accroître les recettes publicitaires de
l'entreprise, à la fois directement, chaque podcast étant sponsorisé par un
annonceur cité au générique, et indirectement par le surcroît de trafic généré
sur le site de l'entreprise. On estime que le projet a une durée de vie
économique de 4 ans.

L'investissement nécessaire en matériel informatique (serveurs principalement)
est estimé à 360 000 euros, amortis en 4 ans en linéaire. Il serait réalisé
au tout début de 2005, la première année d'activité sera donc 2005. Le matériel
utilisé sera sans valeur à la fin du projet.

La production des podcasts proprement dits aura pour effet principal d'augmenter
les charges de personnel de l'entreprise (des embauches sont prévues à la
rédaction pour couvrir les différentes thématiques), ainsi que de générer des frais
supplémentaires en terme de prestations de services. Le service
comptabilité-contrôle de l'entreprise a réalisé des prévisions à ce sujet. On
estime pour simplifier que les charges de personnel supplémentaires seront de
450 000 euros par an, et que les autres dépenses représenteront 23% du chiffre
d'affaires (c'est-à-dire les rentrées publicitaires). On suppose que le BFR
nécessaire à cette nouvelle activité restera stable à 75 000 euros.

Freenews est cotée depuis très peu de temps, ce qui rend difficile l'estimation
statistique de son bêta, mais le directeur financier suit depuis
quelques temps les performances boursières et les caractéristiques d'un
concurrent qui présente des caractéristiques similaires, sauf pour la structure
financière. Ce concurrent a un bêta de 1,8 et un ratio D/C de 42%. Compte tenu
de ces informations, le coût des capitaux propres du concurrent s'établit à
24%.

Un extrait du passif du bilan de l'entreprise Freenews avant investissement est présenté
ci-dessous (en milliers d'euros) :

\begin{center}
\begin{tabular}{lr}
\toprule
Passif du bilan au 31/12/2004 & \\
\otoprule
Capital social & 100~000 \\
Réserves, résultats conservés & 45~000 \\
Emprunt obligataire & 100~000 \\
\bottomrule
\end{tabular}
\end{center}

Le capital social est composé de 10 000 000 d'actions de 10 euros de nominal. Le
dernier cours connu fin 2004 est de 13,80 euros, et il est prévu de verser un
dividende de 3,20 euro par action en juin 2005.

L'emprunt obligataire avait été émis il y a trois ans pour une durée de sept ans,
ses caractéristiques étaient les suivantes :

- 100 000 000 obligations de 1 euro de nominal, émises à 99%
- remboursement in fine à 102%
- coupon de 6%

Le rendement des emprunts d'État est à l'heure actuelle de 4,10%, on estime
que pour sa part, l'entreprise pourrait emprunter à 6,8% actuellement. Le
taux de l'impôt sur les bénéfices est 34%, il s'applique également aux
plus-values de toute nature réalisées par l'entreprise.
