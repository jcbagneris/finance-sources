\thispagestyle{empty}
La société Kaltenbrunner produit de la boulangerie et de la pâtisserie
traditionnelles alsaciennes. Fondée en 1864 par Herbert Kaltenbrunner,
l'entreprise s'est rapidement développée au niveau régional, en raison d'une
politique commerciale agressive et assez innovante pour l'époque.

Aujourd'hui, la société distribue toujours ses produits directement et sous sa
marque, répondant ainsi au désir du fondateur qui avait fait jurer à ses
héritiers de ne pas trahir l'esprit de la maison en vendant ses productions aux
grandes centrales d'achat. Par ailleurs elle est cotée au second marché
d'Euronext Paris et les dirigeants ont toujours souhaité maintenir un ratio de
dettes sur capitaux propres (en valeur de marché) de 0,6.

Une partie des produits est constituée de biscuits secs à la conservation facile
et durable. Pour cette raison, l'actuel PDG, Karl, arrière petit-fils du
fondateur, souhaite développer un canal de distribution vers les particuliers à
partir de commandes passées sur internet.
Il espère un développement non négligeable du chiffre d'affaires de cette
branche au travers de cette innovation.

Karl Kaltenbrunner (que tout le monde appelle K2) a accepté la proposition du
directeur financier de l'entreprise de modifier la structure financière de
l'entreprise pour arriver rapidement à un nouveau ratio D/C de 0,8. Le directeur
financier précise que ce ratio aurait sans doute des conséquences sur le bêta
mais pas sur le coût de l'endettement.

Les flux de liquidité attendus du projet sont résumés dans le tableau
ci-dessous :

\begin{center}
\begin{tabular}{lccccc}
\toprule
Période & 0 & 1 & 2 & 3 & 4 \\
\otoprule
Flux net & -20~000 & 5~000 & 8~000 & 8~000 & 12~000 \\
\bottomrule
\end{tabular}
\end{center}

L'entreprise emprunte habituellement au taux de 7,8% par an. Le taux de l'impôt
sur les bénéfices est 34%. Le directeur financier indique que le bêta de
Kaltenbrunner avant modification du ratio d'endettement est de 1,15. Le
rendement attendu du marché sera cette année de 19,6%. Le taux sans risque
s'établit à 3,80%.
