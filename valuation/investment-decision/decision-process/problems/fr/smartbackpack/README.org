* Used in
Exam session 1 BIBA3A 2023-2024
Exam session 1 BL3C 12-2019


* Topics
- calculate current structure with bonds and stocks price
- change in capital structure, 2 WACCs to calculate
- question about WACC change influence without any calculation
