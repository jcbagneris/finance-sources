#+title: Easyvisa
#+export_file_name: ./_dest/20231020T141921--easyvisa-jc-bagneris__fr_pb_inv

* preamble                                                                                  :ignore:
#+latex: \thispagestyle{empty}

* Text                                                                                      :ignore:
Easyvisa est à l'origine une agence de voyage qui à la suite de difficultés
financières a modifié son activité il y a une dizaine d'années. Elle est
désormais spécialisée dans la facilitation de l'obtention des visas pour les
particuliers (tourisme) mais aussi et surtout pour les entreprises (voyages
d'affaires, prospection). Son originalité et une part importante de son succès
viennent du fait qu'elle fonctionne "dans les deux sens" : elle peut aussi bien
fournir un visa pour la Chine à un cadre français, qu'aider une entreprise
française à inviter un client Vietnamien. Ces deux exemples sont souvent fournis
par le chef d'entreprise parce que l'agence a de très bons résultats avec les
pays d'Asie, mais on pourrait en citer beaucoup d'autres.

Easyvisa envisage un investissement important pour assurer sa présence sur
internet, en particulier pour les clients "tourisme", pour lesquels les
formalités sont souvent assez simples, et un paiement en ligne envisageable.

L'entreprise n'est pas cotée en bourse, son actionnariat a toujours été
familial. Toutefois, le directeur financier estime qu'il peut utiliser pour
calculer son bêta le bêta économique du secteur d'activité "tourisme, tour
operators" qui est de 1,1. Easyvisa a 20% de dettes dans sa structure (ratio
d'endettement) et la valeur de marché de ses capitaux propres est estimée à 2,9
millions d'euros. L'entreprise peut actuellement emprunter au taux de 5,4% par
an. Le rendement attendu du marché financier est de 7,71% cette
année, et le taux sans risque est de 1,98%.

Le projet de développement sur internet demanderait un investissement de 335000
€ pour des EBE attendus résumés dans le tableau ci-dessous:

#+attr_latex: :align lrrrrr :booktabs t
| Année |     1 |      2 |      3 |      4 |      5 |
|-------+-------+--------+--------+--------+--------|
| EBE   | 46000 | 125000 | 244000 | 324000 | 400000 |

Les actifs du projet seront amortis en linéaire sur 5 ans et pourront être
revendus à 19% de leur valeur d'acquisition à la fin de celui-ci. Le taux de
l'impôt sur les bénéfices et sur les plus-values est de 35%. Vous pouvez
considérer que l'entreprise est bénéficiaire sur l'ensemble de ses autres
activités. Le besoin en fonds de roulement supplémentaire lié au projet
représente 11% de l'EBE chaque année, il sera récupéré intégralement au bout des
cinq ans.
* Questions                                                                                 :ignore:
1. Quel est le coût des capitaux propres de Easyvisa ? (en %, deux décimales)
2. Quel est le taux de rendement requis à utiliser pour le projet
   d'investissement ? (en %, deux décimales)
3. Quelle est la valeur du flux de désinvestissement ? (en €, sans décimales)
4. Quelle est la valeur actuelle nette du projet ? (en €, sans décimales)
5. Pour financer l'ensemble de ses projets, l'entreprise envisage un emprunt de
   337000 euros sur 9 ans, au taux auquel elle peut emprunter actuellement,
   remboursé par annuités constantes. Quelle serait alors la valeur de l'annuité
   constante ? (en €, deux décimales)
6. Si le nouvel emprunt est réalisé, que devient le TRR du projet en supposant
   que le coût de la dette n'est pas impacté ? (en %, deux décimales)


* footer                                                                                    :ignore:
#+include: "~/work/writing-helpers/orgmode/layout.org::*footer-std" :minlevel 1 :only-contents t

* locvar                                                                                    :ignore:
# Local Variables:
# time-stamp-line-limit: 3
# time-stamp-start: "^#\\+export_file_name: .*/_dest/"
# time-stamp-format: "%Y%m%dT%H%M%S"
# time-stamp-end: "--"
# End:
