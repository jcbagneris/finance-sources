\thispagestyle{empty}
La société Tradiplant est spécialisée dans la fabrication et la vente de
produits à base de plantes (tisanes, décoctions, etc.). Le directeur financier
étudie la possibilité de remplacer les quatre machines à ensacher automatiques
de l'entreprise au début de l'année 2010. A la fin de l'exercice 2009, les
machines en question, d'une valeur d'acquisition de 50 000 euros pièce, auront
été mises en service depuis 5 ans et auront été amorties à 50% puisque la durée
d'amortissement (linéaire) prévue était de 10 ans. Un représentant du
fournisseur d'équipements a proposé récemment de remplacer ces quatres machines
par deux machines plus modernes, capables de produire le même rendement, et dont
le fonctionnement serait plus économique. Le chiffre d'affaire, la production et
le BFR ne seraient pas affectés par ce changement, qui n'induirait qu'une
économie de charges décrite dans les paragraphes ci-dessous.

Le directeur de l'usine estime que les anciennes machines pourraient fonctionner
encore pendant cinq ans, mais que cela entraînerait chaque année des frais
d'entretien élevés. Il estime que les coûts combinés de fonctionnement et
d'entretien s'éléveraient chaque année à 30 000 euros pour chaque machine. Si on
décide de changer les anciennes machines, on pourrait vendre actuellement chaque
machine au prix de 11 000 euros. Si au contraire on les utilise encore cinq ans,
on estime qu'on ne pourra les vendre dans cinq ans que 500 euros pièce.

Le prix de chaque nouvelle machine s'élève à 125 000 euros selon le fournisseur.
Ces machines seraient amorties en linéaire sur cinq ans.  Les coûts de
fonctionnement et d'entretien de celles-ci sont plus faibles que ceux des
anciennes machines : seulement 25 000 euros (et on rappelle qu'il ne faut que
deux nouvelles machines pour remplacer les quatre anciennes).  Toutefois, au
bout de cinq ans, ces machines seraient bonnes à jeter et n'auraient plus aucune
valeur.

Tradiplant est cotée depuis plusieurs années au Second Marché d'Euronext Paris.

Le capital social est composé de 1 000 000 d'actions de 10 euros de nominal. Le
bêta de l'entreprise est de 1,14. Le dernier cours connu fin 2008 est de 44,7
euros. Le marché financier est par ailleurs très marqué par la crise financière
en 2009 et devrait terminer l'année sur un rendement de 12,70%.

La banque conseil de l'entreprise indique que celle-ci pourrait s'endetter
aujourd'hui au taux de 7,30%.

Le directeur financier estime que la structure financière de l'entreprise est
caractérisée par un ratio \mbox{dettes/capitaux propres} de 50%.

Le rendement des emprunts d'État est à l'heure actuelle de 2,70%.
Le taux de l'impôt sur les bénéfices est 34%, il s'applique également aux
plus-values de toute nature réalisées par l'entreprise.

