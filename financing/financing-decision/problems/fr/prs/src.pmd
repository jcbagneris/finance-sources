La société PRS a été créée en 2003. Son fondateur avait à l'époque investi
230 000 € et reçu 920 000 actions de préférence de la série A. L'entreprise a
ensuite augmenté ses capitaux propres à deux reprises :

\begin{minipage}{\textwidth}
\centering
\begin{tabular}{lllcc}
Année	& Série		& Prix (€)	& Nombre d'actions		& Investisseur			\\\otoprule
2005	& Série B	&	0,60		&   450~000			& Amis du fondateur	 	\\
2008	& Série C	&	3,10		&   300~000			& Soc. de capital risque\\
\bottomrule
\end{tabular}
\end{minipage}
\vspace{\parskip}

Fin 2011, les trois séries d'actions de préférences ont été converties en
actions ordinaires, sur la base de 1 pour 1.
En 2012, la société a été introduite en bourse. Un total de 1 400 000 actions ont
été vendues au public, dont 1 100 000 actions nouvelles et les 300 000 actions de la
société de capital risque entrée en 2008. L'entreprise a choisi un contrat de
prise ferme avec sa banque : les actions étaient offertes au public à 9,40 €
mais la banque a prélevé 0,25 € de frais d'émission par action. A ces frais se
sont ajoutées des dépenses directement liées à l'émission pour 680 000 €.

En mars 2014, l'entreprise envisage de nouveaux investissements pour lesquels
elle doit trouver un financement net de 6 000 000 €. Les opportunités de financement
pour trouver ce montant sont les suivantes :

- Une augmentation de capital. Le prix d'émission serait de 18,04 € et les
  frais d'émission représenteraient 3,9439% du montant brut de celle-ci.
- Une émission d'obligations convertibles de maturité 4 ans. Le taux de
  coupon serait de 3,40% et chaque obligation serait convertible en 2
  actions ordinaires à tout moment. En cas de non conversion, les obligations
  seraient remboursées à 53 €. Les obligations seront émises au pair à
  45 € et les frais d'émission représenteraient 3,7304% du montant brut de
  l'émission.

Le cours boursier de l'action juste avant l'opération était de 21,15 €.
