\thispagestyle{empty}
La société DNA a été créée en 1999 pour éditer l'un des premiers quotidiens
totalement gratuits de France. Les recettes sont uniquement publicitaires. En
dehors de cela, le journal fonctionne comme n'importe quel autre quotidien.
Début 2015, les dirigeants de DNA envisagent sérieusement d'investir pour
consolider leur part de marché sur internet. Pour financer ce développement
(principalement des investissements et l'embauche de nouveaux collaborateurs),
la société aurait besoin d'un financement (net) de 2 500 000 €.

Les possibilités de financement sont les suivantes :

- Une augmentation de capital avec droits de souscription. Le prix
  d'émission des actions nouvelles serait 15,20 € et les frais d'émission
  représenteraient 1,9063% du montant brut de celle-ci.

- Une émission d'obligations convertibles de maturité 4 ans. Le taux de
  coupon serait de 4,60% et chaque obligation serait convertible en 4
  actions ordinaires à tout moment. Si elles n'étaient pas converties, les
  obligations seraient remboursées au pair. Le prix d'émission et la valeur
  nominale seraient de 118 € et les frais d'émission s'élèveraient à 3,1256% du
  montant brut de celle-ci.

La société DNA est cotée à la bourse de Paris (Euronext) depuis peu de temps. De
ce fait, il n'y a que peu d'historique sur les cours boursiers, et il n'est pas
possible de calculer le bêta par des méthodes statistiques. Le directeur
financier a toutefois récupéré des informations sur un concurrent aux activités
similaires, mais avec un financement différent. Le bêta du concurrent est 1,5 et
son levier financier (D/C) 30%. DNA a 1 509 030 actions en circulation avant le
nouveau financement. A la fin de l'année 2014 le cours boursier était de 21,40 €
et un dividende de 3,20 € par action sera payé en juin 2015.

La seule dette long terme de DNA est une émission obligataire. 150 000 obligations de
100 € de valeur nominale ont été émises il y a 3 ans pour 7 ans. Elles seront
remboursées au pair et leur taux de coupon est de 6,50%. Les coupons sont
payés annuellement. Le rendement actuariel (YTM) des dettes de l'entreprise est
6,80% actuellement, c'est également le taux auxquel l'entreprise pourrait se
réendetter si nécessaire.

Le taux sans risque est actuellement de 2,90% et le rendement attendu du
marché 12,40%. Le taux de l'impôt sur les bénéfices est 1/3.
