\thispagestyle{empty}
\vspace{-1.6\parskip}
Norbike Inc. is a spare parts specialist for the bicycles and mountain bikes
market. It was established in 2002 and at that time, the founder invested
180,000 € and received 500,000 shares of series A stock. The company then went
through 2 additional rounds of financing:

\begin{minipage}{\textwidth}
\centering
\begin{tabular}{lllcc}
Year	& Round		& Price (€)	& Number of Shares	& Investor				\\\otoprule
2004	& Series B	&	1.50		&   200,000			& Angels	 			\\
2007	& Series C	&	2.80		&   280,000			& Venture Capitalists	\\
\bottomrule
\end{tabular}
\end{minipage}

In the end of 2010, all 3 series of preferred shares were converted in common shares
of stock on a 1 for 1 basis. The company then went public through an IPO. A
total of 1,700,000 shares were sold to the public, 1,420,000 primary shares and
the 280,000 shares of the Venture Capitalists who left the company at that time.
The issuing price was 6.50 € per share. The underwriter fee was 0.20 € per share
and the direct costs 170,000 €.

In March 2015, Norbike needs additional resources to finance its growth. At that
time, the company has debt already and its leverage is 0.4. The net
expected proceeds (after issuing and direct costs) should be 4,000,000 €. The
financing opportunities are as follows:

- A SEO of new common stocks. The issuing price would be 13.18 €, the issuing
  costs will represent 3.196% of the gross proceeds of the issue and there will
  be additional direct costs of 82,805 €.
- A 5-years convertible bonds issue. Coupon rate would be 3.4% and each bond
  would be convertible into 3 common stocks at any time. The bonds would be
  issued and repaid at par at 53 € and the issuing costs will be 3.535% of the gross
  proceeds of the issue, plus 90,116 € of direct costs.

The stock price in March 2015 just before the new financing was 15.45 €. The
unlevered beta of the company's activity is 1.05. The marginal cost of its debt
is 7.5% before tax, and the corporate tax rate is 1/3. The risk free rate is
2.8% and the market risk premium 8.7%.
