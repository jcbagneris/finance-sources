Norbike Inc. is a spare parts specialist for the bicycles and mountain bikes
market. It settled down in the North of France some 10 years ago, and has been
quite successful since. A substantial share of its sales is exported to Belgium.

The balance sheet as of December 31, 2008 (before dividend decision) is as
follows ('000 euros) :

\begin{center}
\begin{tabular}{lr|lr}
\toprule
Assets & & Liabilities & \\
\otoprule
\textbf{Current assets}	&			&\textbf{Current liabilities}&			\\
Accounts receivable		&  15,000 	& Cash deficit				&   5,500 	\\
Inventory				&  20,000	& Accounts payable 			&  16,000	\\
\textbf{Fixed assets}	&			& \textbf{Debt} 			&			\\
Net plant and equipments& 270,000	& Long term debt	 		&  40,000	\\
						&			& \textbf{Owner's equity} 	& 			\\
						&			& Common stocks (1) 		& 200,000 	\\
						&			& Retained earnings			&  29,000	\\
						&			& 2008 Net earnings 		&  14,500 	\\
\otoprule
\textbf{Total} 			& 305,000	& \textbf{Total} 			& 305,000 	\\
\bottomrule
\multicolumn{4}{l}{(1) 2,000,000 stocks} \\
\end{tabular}
\end{center}

All data is in '000 euros.

Norbike whishes to invest in order to sustain its development. The forecasted
investment program (equipments) is as follows: 80,000 for 2009, 80,000 for 2010,
and 40,000 for 2012. Investments are realized at the beginning of the year, thus
depreciation starts at the end of same year.

Exerpts of 2008 income statement:
\begin{center}
\begin{tabular}{lr}
\toprule
Sales & 150,000 \\
Operating expenses & 92,000 \\
Depreciation & 33,000 \\
Interest paid & 3,250 \\
Net earnings & 14,500 \\
\bottomrule
\end{tabular}
\end{center}

Activity forecast:

- Turnover should be 180,000 in 2009, then 200,000 per year for 2 years, then
  210,000 in 2012. Operating expenses rate is supposed to stay the same.
- Working capital requirements should represent 45 days'sales.
- Long term debt will be repaid on a 10,000 per year basis, interest rate is 6.5\%.
- Dividends should be 30% of net earnings, including in 2008. Dividends are paid
  the year following decision.
- Corporate tax rate is 1/3.
- New fixed assets should be depreciated on a 5 years straight line basis.
  Depreciation in 2008 covers in place fixed assets and will stay the same in
  the foreseeable future.

