**Partie 1**

Présenter les comptes de résultat, le tableau des flux de trésorerie et les bilans
prévisionnels pour les 4 premières années d'activité.

**Partie 2**

Répondez aux questions suivantes :

- Combien faut-il vendre de groupes par an pour être rentable ?
  Quels problèmes sont posés par cette question ?
- Supposez qu'en fait on ait 2 000 de coûts variables par groupe pour 210 000 de
  charges fixes. Que cela change-t-il ? La situation vous paraît-elle plus ou
  moins risquée qu'avec la répartition initiale ? Et qu'en est-il si on a 5 000
  de coûts variables pour 60 000 de charges fixes ?
- Supposez qu'au bilan de départ il n'y a que 70 000 de capitaux propres pour
  60 000 de dettes (aux mêmes conditions). Que cela change-t-il ?
- Le principal client de l'entreprise représente 60% de son chiffre d'affaires.
  Imaginez que ce client change de fournisseur et quitte Generator en année 3.
  Que se passera-t-il ?
- Un tel évènement est improbable (vraiment ?). En revanche, ce client, fort de
  son poids, négocie une remise de 30% sur chaque groupe qu'il achète. Qu'en
  pensez-vous ?
- L'entreprise dépend d'un seul fournisseur pour la matière première principale,
  qui représente 70% des coûts variables. Quel serait l'impact d'une hausse des
  prix de ce fournisseur de 30% sur la rentabilité de Generator ? Que
  recommandez-vous ?
- On a sous-estimé la longueur du cycle de production et le BFR s'établira en
  fait aux environs de 90 jours par an. Y'a-t-il un impact sur la rentabilité de
  l'entreprise ? Quel serait l'impact sur la trésorerie ? Sur le financement ?
