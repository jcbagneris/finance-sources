Links
-----

https://en.wikipedia.org/wiki/Bookkeeping
https://en.wikipedia.org/wiki/Double-entry_bookkeeping_system
https://en.wikipedia.org/wiki/Debits_and_credits
https://en.wikipedia.org/wiki/Chart_of_accounts
https://en.wikipedia.org/wiki/Accounting
https://en.wikipedia.org/wiki/Financial_statement

Demo company example spreadsheet
--------------------------------

https://docs.google.com/spreadsheets/d/1uT6XAnD54TLvgJwVFrmTVSgmtqJG96uPS9tIZIWducU/edit?usp=sharing
